import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {SafeHtmlPipe} from "./safe-html.pipe";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MatCommonModule} from "@angular/material/core";
import {JobPostingsComponent} from './job-postings/job-postings.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {PostJobPanelComponent} from './post-job-panel/post-job-panel.component';
import {ApplyJobPanelComponent} from './apply-job-panel/apply-job-panel.component';
import {MatSelectModule} from "@angular/material/select";
import {QuillModule} from "ngx-quill";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatChipsModule} from "@angular/material/chips";
import {ViewJobComponent} from './view-job/view-job.component';
import { ApplicationsComponent } from './applications/applications.component';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		ActionBarComponent,
		ConfigPanelComponent,
		SafeHtmlPipe,
		JobPostingsComponent,
		PostJobPanelComponent,
		ApplyJobPanelComponent,
		ViewJobComponent,
		ApplicationsComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatCommonModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		MatCardModule,
		MatInputModule,
		MatDialogModule,
		MatTableModule,
		MatPaginatorModule,
		MatProgressSpinnerModule,
		MatSelectModule,
		QuillModule.forRoot(),
		MatAutocompleteModule,
		MatChipsModule,
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
