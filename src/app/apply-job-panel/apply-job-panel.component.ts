import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CareersService} from "../careers.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {finalize} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-apply-job-panel',
	templateUrl: './apply-job-panel.component.html',
	styleUrls: ['./apply-job-panel.component.scss']
})
export class ApplyJobPanelComponent implements OnInit {

	private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');
	public uploading: boolean;
	public applicationForm: FormGroup = new FormGroup({
		id: new FormControl(),
		job_id: new FormControl('', Validators.required),
		name: new FormControl('', Validators.required),
		phone: new FormControl(),
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),
		resume: new FormControl(),
		status: new FormControl()
	});


	constructor(
		private careersService: CareersService,
		@Inject(MAT_DIALOG_DATA) public data,
		private dialogRef: MatDialogRef<ApplyJobPanelComponent>,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.applicationForm.controls.job_id.patchValue(this.data?.job.id);
	}

	public upload(event): void {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.careersService.upload(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.applicationForm.controls.resume.patchValue(res);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public deleteFile(): void {
		this.applicationForm.controls.resume.patchValue(null);
	}

	public submit() {
		this.applicationForm.markAllAsTouched();

		if(this.applicationForm.valid) {
			this.careersService.saveApplication(this.applicationForm.getRawValue()).subscribe(res => {
				this.snackBar.open('Application', 'Submitted');
				this.dialogRef.close({new: true, item: res})
			}, error => this.snackBar.open(
				get(error, 'error.message', 'An Error Occurred'),
				'Error',
				{panelClass: 'error-snack'}));
		}
	}
}
