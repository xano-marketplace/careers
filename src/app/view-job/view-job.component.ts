import {Component, OnInit} from '@angular/core';
import {CareersService} from "../careers.service";
import {ActivatedRoute, Router} from "@angular/router";
import {finalize, switchMap} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';
import {cities} from "../cities";
import {ConfigService} from "../config.service";
import {MatDialog} from "@angular/material/dialog";
import {ApplyJobPanelComponent} from "../apply-job-panel/apply-job-panel.component";
import {PostJobPanelComponent} from "../post-job-panel/post-job-panel.component";

export interface Job {
    id: number,
    created_at: number,
    title: string,
    description: string,
    type: string,
    location: any,
    category: any[]
}

@Component({
	selector: 'app-view-job',
	templateUrl: './view-job.component.html',
	styleUrls: ['./view-job.component.scss']
})
export class ViewJobComponent implements OnInit {
    public job: Job;
    public notFound: boolean;
    public cities = cities;
    public viewAsApplicant: boolean;

	constructor(
	    private careersService: CareersService,
        private configService: ConfigService,
        private route: ActivatedRoute,
        private snackbar: MatSnackBar,
		private dialog: MatDialog,
		private router: Router
    ) {
	}

	ngOnInit(): void {

		this.configService.viewAsApplicant.subscribe(res => this.viewAsApplicant = !!res);

	    this.route.params
            .pipe(
                switchMap((params)=> this.careersService.getJob(params.id)),
                finalize(()=> this.notFound = !!this.job)
            )
            .subscribe(res => {
              this.job = res
            }, error => {

                  this.snackbar.open(
                      get(error, 'error.message', 'An error occurred'),
                      'Error',
                      {panelClass: 'error-snack'})
            });

	}

	public getLocation(lat, lng): string {
		let location = this.cities.find(x => x.latitude == lat && x.longitude == lng);

		if (location) {
			return location.city + ', ' + location.state;
		} else {
			return '';
		}
	}

	public openPanel(type) {
		if(type === 'apply') {
			this.dialog.open(ApplyJobPanelComponent, {data: {job: this.job}})
		} else if(type === 'edit') {
			const dialogRef = this.dialog.open(PostJobPanelComponent, {data: {job: this.job}})

			dialogRef.afterClosed().subscribe(res =>{
				if(res?.updated) {
					this.job = res.item;
				} else if(res?.deleted) {
					this.router.navigate([''])
				}
			})
		}
	}
}
