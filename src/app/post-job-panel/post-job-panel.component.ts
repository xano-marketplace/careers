import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {CareersService} from "../careers.service";
import {ConfigService} from "../config.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cities} from "../cities";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {MatAutocomplete, MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {map, startWith} from "rxjs/operators";
import {MatChipInputEvent} from "@angular/material/chips";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-post-job-panel',
	templateUrl: './post-job-panel.component.html',
	styleUrls: ['./post-job-panel.component.scss']
})
export class PostJobPanelComponent implements OnInit {

	public jobForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		title: new FormControl('', Validators.required),
		description: new FormControl(''),
		location: new FormControl('', Validators.required),
		type: new FormControl('full-time'),
	});

	readonly separatorKeysCodes: number[] = [ENTER, COMMA];

	public categories = new Set();
	public availableCategories = [];
	public categoryControl: FormControl = new FormControl('');
	public filteredCategories: Observable<string[]>;
	public location: any;
	@ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
	@ViewChild('autocomplete') matAutocomplete: MatAutocomplete;


	public modules = {
		toolbar: [
			['bold', 'italic', 'underline'],
			['blockquote'],
			[{header: 1}, {header: 2}],
			[{list: 'ordered'}, {list: 'bullet'}],
			[{color: []}],
			['link']
		]
	};

	public cities = cities;

	constructor(
		private careersService: CareersService,
		private configService: ConfigService,
		private dialogRef: MatDialogRef<PostJobPanelComponent>,
		private snackbar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data
	) {

		this.filteredCategories = this.categoryControl.valueChanges.pipe(
			startWith(null, ''),
			map((category: any) => category ? this.filterCategories(category) : this.availableCategories.filter((x) => {
				return !this.categories.has(x);
			})));
	}

	ngOnInit(): void {
		this.careersService.getCategories().toPromise().then(res => {
			for (const category of res) {
				category.category_id = category.id;
			}
			this.availableCategories = res;
		}).then(x => {
			if(this.data?.job) {
				this.jobForm.patchValue(this.data.job);
				this.location = this.cities.find(x => this.data.job.location.data.lat && this.data.job.location.data.lng === x.longitude);

				if(this.data.job.category.length) {
					for(let x of this.data.job.category) {
						let category = this.availableCategories.find(y =>  y.category_id === x.category_id);
						if(category) {
							this.categories.add(category)
						}
					}
				}

			}
		});
	}

	addCategory(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;
		const category = this.availableCategories.find(x => x.category === value);

		if (value && category) {
			this.categories.add(category);
		}

		if (input) {
			input.value = '';
		}

		this.categoryControl.setValue(null);
	}

	removeCategory(category): void {
		this.categories.delete(category);
	}

	selectedCategory(event: MatAutocompleteSelectedEvent): void {
		const category = this.availableCategories.find(x => x.category === event.option.value.category);

		if (category) {
			this.categories.add(category);
		}
		this.categoryInput.nativeElement.value = '';
		this.categoryControl.setValue(null);
	}

	public submit(): void {
		this.jobForm.markAllAsTouched()
		if (this.jobForm.valid) {
			const location = this.jobForm.controls.location;
			if(!location.value?.data) {
				location.patchValue({
					type: 'point',
					data: {lat: location.value.latitude, lng: location.value.longitude}
				})
			}
			if(this.data?.job) {
				this.careersService.updateJob({
					...this.jobForm.getRawValue(),
					category: [...this.categories],
				}).subscribe(res => {
						this.snackbar.open('Job', 'Updated');
						this.dialogRef.close({updated: true, item: res})
				},error =>this.snackbar.open(
					get(error, 'error.message', 'An Error Occurred'),
					'Error',
					{panelClass: 'error-snack'})
				)
			} else {
				this.careersService.saveJob({
					category: [...this.categories],
					...this.jobForm.getRawValue()
				}).subscribe(res => {
					this.snackbar.open('Job', 'Posted');
					this.dialogRef.close({new: true, item: res})
				}, error => this.snackbar.open(
					get(error, 'error.message', 'An Error Occurred'),
					'Error',
					{panelClass: 'error-snack'})
				)
			}
		}
	}

	public deleteJob(): void {
		if(this.data?.job) {
			this.careersService.deleteJob(this.data.job.id).subscribe(res => {
				this.snackbar.open('Job', 'Deleted');
				this.dialogRef.close({deleted: true, item: res})
			}, error => this.snackbar.open(
				get(error, 'error.message', 'An Error Occurred'),
				'Error',
				{panelClass: 'error-snack'}))
		}
	}

	private filterCategories(value: any): any[] {
		const filterValue = typeof value === 'string' ? value.toLowerCase() : value.category.toLowerCase();

		return this.availableCategories.filter(category => {
			return category.category.toLowerCase().indexOf(filterValue) === 0 && !this.categories.has(category);
		});
	}

	public setLocation() {

	}
}
