import {Component, OnInit, ViewChild} from '@angular/core';
import {CareersService} from "../careers.service";
import {ActivatedRoute, Router} from "@angular/router";
import {debounceTime, distinctUntilChanged, finalize, switchMap, tap} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {ConfigService} from "../config.service";
import {MatTable} from "@angular/material/table";

@Component({
	selector: 'app-applications',
	templateUrl: './applications.component.html',
	styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {
	public applications;
	public noResults: boolean;
	public loading: boolean;
	public displayedColumns: any = ['id', 'job_id', 'name', 'contact', 'resume', 'created_at', 'status']
	public search: FormControl = new FormControl('');
	public jobID;


	@ViewChild(MatTable) table: MatTable<any>;


	public currentPage: number;
	public totalItems: number;

	constructor(
		private careersService: CareersService,
		private configService: ConfigService,
		private router: Router,
		private route: ActivatedRoute,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.viewAsApplicant.asObservable().subscribe(res=> {
			if(res) {
				this.router.navigate([''])
			}
		})
		this.route.params
			.pipe(switchMap((params) => {
				this.jobID = params?.id;
				return this.getApplications(null).pipe(
					tap(()=> this.loading = true),
					finalize(()=> {
						this.loading = false;
						this.noResults = this.applications.length == 0;
					}))
			}))
			.subscribe(res => {
				this.applications = res?.items;
				this.currentPage = res.curPage;
				this.totalItems = res.itemsTotal;
			}, error => this.snackBar.open(
				get(error, 'error.message', 'An Error Occurred'),
				'Error',
				{panelClass: 'error-snack'})
			);

			this.search.valueChanges.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				switchMap(()=> this.getApplications(null))
			).subscribe(res => {
				this.currentPage = res.curPage;
				this.totalItems = res.itemsTotal;
				this.applications = res.items;
			});
	}

	public getApplications(page = null): Observable<any> {
		const search = {
			page: page,
			expression: []
		};

		const searchValue = this.search.value?.trim();

		if (searchValue) {
			search.expression.push({
				'type': 'group',
				'group': {
					'expression': [
						{
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'application.name'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						}
					]
				}
			});
		} else if(this.jobID) {
			search.expression.push({
				'statement': {
					'left': {
						'tag': 'col',
						'operand': 'application.job_id'
					},
					'right': {
						'operand': `${this.jobID}`
					}
				}
			})
		}

		return this.careersService.getApplications(search)
	}

	public updateStatus(application, event) {
		application.status = event.value
		this.careersService.updateApplication(application).subscribe(res =>{
			this.snackBar.open('Application Status', 'Updated')
		}, error =>  this.snackBar.open(
			get(error, 'error.message', 'An Error Occurred'),
			'Error',
			{panelClass: 'error-snack'})
		);

	}

	public pageChangeEvent(event): void {
		this.getApplications( event.pageIndex + 1).subscribe(res => {
			this.applications = res?.items;
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
		});
	}

	public resetSearch() {
		this.search.reset();
		this.getApplications(null).subscribe(res => {
			this.applications = res?.items;
			this.table.renderRows();
			this.currentPage = res.curPage;
			this.totalItems = res.itemsTotal;
		})
	}
}
